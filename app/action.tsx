'use server';

import AnimeCard, { AnimeProp } from "@/components/AnimeCard";

const URL = "";
export const fetchAnime = async(page:number)=>{
  const result = await fetch(
    `https://shikimori.one/api/animes?pages=${page}&limit=8&order=popularity`
    );
  const data = await result.json();
  console.log(data);
  return data.map((item: AnimeProp, index:number) => (
    <AnimeCard key={item.id} anime={item} index={index} />
  ));
}